<?php 
// No direct access
defined('_JEXEC') or die('Restricted access');
?>

<script>
function handleClick(triggered){
	triggered.parentNode.style.display = "none";
}
</script>

<div id="stickleft-container">
	<?php
	$data = $params->get('data');
	$opening = "<div class=\"stickleft-article\">";
	$closing = "<a href=\"javascript:void(0)\" onClick=\"handleClick(this)\">Nascondi</a></div>";
	
	echo $opening;
	foreach (preg_split("/((\r?\n)|(\r\n?))/", $data) as $line){
		if ($line=="\$stop\$")
			echo $closing.$opening;
		else
			echo $line;
	}
	echo $closing;
	
	?>
</div>
